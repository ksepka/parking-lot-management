package com.example.llama.parkinglot;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.widget.Button;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends Activity {

    private Button mInputCar;
    private Button mLookupCar;

    private Intent mIntent;

    private ImageView mLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mLogo = new ImageView(this);
        mLogo.setImageResource(R.drawable.city);

        mInputCar = (Button)findViewById(R.id.input_car);
        mInputCar.setOnClickListener(new View.OnClickListener(){

            public void onClick(View v){
                mIntent = new Intent(MainActivity.this, InputCar.class);
                startActivity(mIntent);
                finish();
            }
        });

        mLookupCar = (Button)findViewById(R.id.lookup_car);
        mLookupCar.setOnClickListener(new View.OnClickListener(){

            public void onClick(View v){
                mIntent = new Intent(MainActivity.this, LookupCar.class);
                startActivity(mIntent);
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
